/*
 * positMath_cpp.h
 *
 *  Created on: Nov 26, 2018
 *      Author: cerlane
 */

#ifndef INCLUDE_POSITMATH_CPP_H_
#define INCLUDE_POSITMATH_CPP_H_

#include "positMath.h"
#include "softposit_cpp.h"

//max
inline posit8 max(posit8 a, posit8 b){
	posit8 ans;
	ans.value = castUI( p8_max(castP8(a.value), castP8(b.value)) );
	return ans;
}

inline posit16 max(posit16 a, posit16 b){
	posit16 ans;
	ans.value = castUI( p16_max(castP16(a.value), castP16(b.value) ) );
	return ans;
}

inline posit32 max(posit32 a, posit32 b){
	posit32 ans;
	ans.value = castUI( p32_max(castP32(a.value), castP32(b.value) ) );
	return ans;
}

//min
inline posit8 min(posit8 a, posit8 b){
	posit8 ans;
	ans.value = castUI( p8_min(castP8(a.value), castP8(b.value)) );
	return ans;
}

inline posit16 min(posit16 a, posit16 b){
	posit16 ans;
	ans.value = castUI( p16_min(castP16(a.value), castP16(b.value)) );
	return ans;
}

inline posit32 min(posit32 a, posit32 b){
	posit32 ans;
	ans.value = castUI( p32_min(castP32(a.value), castP32(b.value)) );
	return ans;
}

//abs
inline posit8 abs(posit8 a){
	posit8 ans;
	ans.value = castUI( p8_abs(castP8(a.value)) );
	return ans;
}

inline posit16 abs(posit16 a){
	posit16 ans;
	ans.value = castUI( p16_abs(castP16(a.value)) );
	return ans;
}

inline posit32 abs(posit32 a){
	posit32 ans;
	ans.value = castUI( p32_abs(castP32(a.value)) );
	return ans;
}


//ceil
inline posit8 ceil(posit8 a){
	posit8 ans;
	ans.value = castUI( p8_ceil(castP8(a.value)) );
	return ans;
}

inline posit16 ceil(posit16 a){
	posit16 ans;
	ans.value = castUI( p16_ceil(castP16(a.value)) );
	return ans;
}

inline posit32 ceil(posit32 a){
	posit32 ans;
	ans.value = castUI( p32_ceil(castP32(a.value)) );
	return ans;
}

//floor
inline posit8 floor(posit8 a){
	posit8 ans;
	ans.value = castUI( p8_floor(castP8(a.value)) );
	return ans;
}

inline posit16 floor(posit16 a){
	posit16 ans;
	ans.value = castUI( p16_floor(castP16(a.value)) );
	return ans;
}

inline posit32 floor(posit32 a){
	posit32 ans;
	ans.value = castUI( p32_floor(castP32(a.value)) );
	return ans;
}

//log
inline posit8 log(posit8 a){
	posit8 ans;
	ans.value = castUI( p8_log(castP8(a.value)) );
	return ans;
}

inline posit16 log(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_log(castP16(a.value)) );
    return ans;
}

//log2
inline posit16 log2(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_log2(castP16(a.value)) );
    return ans;
}

//exp
inline posit8 exp(posit8 a){
    posit8 ans;
    ans.value = castUI( p8_exp(castP8(a.value)) );
    return ans;
}

inline posit16 exp(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_exp(castP16(a.value)) );
    return ans;
}

//exp2
inline posit16 exp2(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_exp2(castP16(a.value)) );
    return ans;
}

//sinpi
inline posit16 sinpi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_sinpi(castP16(a.value)) );
    return ans;
}

//cospi
inline posit16 cospi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_cospi(castP16(a.value)) );
    return ans;
}

//tanpi
inline posit16 tanpi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_tanpi(castP16(a.value)) );
    return ans;
}

//asinpi
inline posit16 asinpi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_asinpi(castP16(a.value)) );
    return ans;
}

//acospi
inline posit16 acospi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_acospi(castP16(a.value)) );
    return ans;
}

//atanpi
inline posit16 atanpi(posit16 a){
    posit16 ans;
    ans.value = castUI( p16_atanpi(castP16(a.value)) );
    return ans;
}

#endif /* INCLUDE_POSITMATH_CPP_H_ */
