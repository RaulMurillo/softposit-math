/*
 * positMath.h
 *
 *  Created on: Jun 26, 2018
 *      Author: cerlane
 */


#include "softposit.h"

#ifdef SOFTPOSIT_QUAD
#include <quadmath.h>
#endif


#ifndef INCLUDE_SOFTPOSIT_MATH_H_
#define INCLUDE_SOFTPOSIT_MATH_H_


#ifdef __cplusplus
extern "C"{
#endif

posit32_t p32_floor( posit32_t );
posit32_t p32_abs( posit32_t );
posit32_t p32_ceil( posit32_t );
posit32_t p32_max (posit32_t, posit32_t);
posit32_t p32_min (posit32_t, posit32_t);

posit16_t p16_abs( posit16_t);
posit16_t p16_exp( posit16_t );
posit16_t p16_exp2( posit16_t );
posit16_t p16_log( posit16_t );
posit16_t p16_log2( posit16_t );
posit16_t p16_sinpi( posit16_t );
posit16_t p16_cospi( posit16_t );
posit16_t p16_tanpi( posit16_t );
posit16_t p16_asinpi( posit16_t );
posit16_t p16_acospi( posit16_t );
posit16_t p16_atanpi( posit16_t );
posit16_t p16_floor( posit16_t );
posit16_t p16_ceil( posit16_t );
posit16_t p16_max (posit16_t, posit16_t);
posit16_t p16_min (posit16_t, posit16_t);

posit8_t p8_abs( posit8_t);
posit8_t p8_exp( posit8_t );
posit8_t p8_log( posit8_t );
posit8_t p8_floor( posit8_t );
posit8_t p8_ceil( posit8_t );
posit8_t p8_max (posit8_t, posit8_t);
posit8_t p8_min (posit8_t, posit8_t);



#ifdef __cplusplus
}
#endif

#endif

