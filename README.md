# SoftPosit-Math

This will in time (hopefully) contains all the libMath functions for posits.

Math functions added for 16-bit posits:
*  exp
*  exp2
*  log
*  log2
*  sinpi & asinpi
*  cospi & acospi
*  tanpi & atanpi

## Acknowledgements

John Gustafson for the algorithm designs for the math functions.

# To build

## Linux and OS-X

#### Option 1:
```
cd softposit-math/build
./make.sh
```

This will automatically download and build SoftPosit, and compile SoftPosit-Math.

To build the combined single library containing SoftPosit, use:

```
make single-lib
```

The library can be tested for correctness:

```
make check
```

The micro-benchmarks (OpenMP required) can be built with:

```
make benchmarks
```

#### Option 2: Using Spack

A Spack repo for SoftPosit-Math and SoftPosit is available [here](https://gitlab.com/jlow_2019/softposit-spack-repo.git).

(1) Clone the repo and import it:

```
spack repo add softposit-spack-repo
```

(2) Build and install the SoftPosit-Math library along with the SoftPosit lib:

```
spack install softposit-math
```

## Windows

(1) Download [SoftPosit](https://gitlab.com/cerlane/SoftPosit)  and install it. 

(2) Download SoftPosit-Math.

(3) To install: Set environment variable "SOFTPOSIT"

```
set SOFTPOSIT={DIR}\SoftPosit
```

(4) To install: Compile

```
cd {DIR}\softposit-math\build
make all

```

# To use

For example, if you have a file "main.c" that uses both SoftPosit-Math and SoftPosit, you can compile your code as such:

```
gcc -lm  -o main main.c {DIR}/softposit-math/build/softposit_math.a \
      {DIR}/SoftPosit-master/build/Linux-x86_64-GCC/softposit.a -O2 \
    -I{DIR}/SoftPosit-master/build/Linux-x86_64-GCC \
    -I{DIR}/SoftPosit-master/source/include \
    -I{DIR}/softposit-math/include 
```

or with the single library:

```
gcc -lm  -o main main.c {DIR}/softposit-math/build/softposit_math.a -O2 \
    -I{DIR}/SoftPosit-master/build/Linux-x86_64-GCC \
    -I{DIR}/SoftPosit-master/source/include \
    -I{DIR}/softposit-math/include
```

The micro-benchmarks can be run with the desired number of OpenMP threads:

```
OMP_NUM_THREADS=4 ./benchmark-p16_exp2.exe
```
