
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low.

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the University nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS", AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "softposit.h"
#include "softposit_types.h"
#include "positMath.h"

#include <stdio.h>
#include <math.h>

void displayBinary(uint64_t * s, int size);

int main (int argc, char *argv[]){

    posit16_t pA, pA_roundedToInt, pB, pZ;

    int i=0, count=0;
    double dA, dZ;
    uint16_t uiZ;
    bool test = 0;

    // All posit numbers
    for (i=0; i<65536; i++)	{

    	pA.v = i;
    	pZ = p16_sinpi(pA);
    	dA = convertP16ToDouble(pA);

    	// Code for posits as exact integers
    	pA_roundedToInt = p16_roundToInt(pA);
    	if (p16_eq(pA_roundedToInt, pA)) {
            pB.v = 0;
    	} else {
    	    dZ = sin(acos(-1.0) * dA);
            pB = convertDoubleToP16(dZ);
    	}

    	// Code for NaR case
    	if (i == 32768) pB.v = 32768;

    	test = p16_eq(pZ, pB);
        if (!test) {
        	printf("Error at posit %d (%f):  Result is", i, dA);
        	displayBinary((uint64_t*)&castUI(pZ), 16);
        	printf(" (Value %f)\n", convertP16ToDouble(pZ));
        	printf("Error at posit %d (%f):  Should be", i, dA);
        	displayBinary((uint64_t*)&castUI(pB), 16);
        	printf(" (Value %f)\n", dZ);
        	printf("\n");
        	count++;
        }
    }


    if (count) {
    	printf("\nTest failed for %d 16-bit posits of sinpi !\n", count);
    	return 1;
    } else {
    	printf("Test passed for all sinpi(posit16) !\n");
    	return 0;
    }

}

void displayBinary(uint64_t * s, int size) {
	int i;
	uint64_t number = *s;
	int bitSize = size -1;
	for(i = 0; i < size; ++i) {
		if(i%8 == 0)
			putchar(' ');
		printf("%lu", (number >> (bitSize-i))&1);
	}

}
