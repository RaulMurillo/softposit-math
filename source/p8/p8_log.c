/*============================================================================

This C source file is part of the SoftPosit Posit Arithmetic Package
by S. H. Leong (Cerlane) and John Gustafson

Copyright 2018 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the University nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS", AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "positMath.h"


posit8_t p8_log( posit8_t pA ) {
	const uint8_t log8 [128] = {128, 144, 148, 152, 154, 156, 157, 158, 159, 161, 165, 168, 170,
			173, 175, 178, 180, 182, 183, 185, 187, 188, 190, 191, 193, 196, 198,
			201, 203, 205, 208, 210, 212, 214, 216, 217, 219, 221, 223, 224, 226,
			228, 229, 231, 232, 233, 235, 236, 238, 239, 240, 241, 243, 244, 245,
			246, 247, 249, 250, 251, 252, 253, 254, 255, 0, 2, 4, 6, 8, 9, 11,
			13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 27, 29, 30, 31, 32, 33, 35,
			36, 37, 38, 39, 40, 41, 42, 43, 44, 48, 52, 55, 59, 62, 64, 66, 67,
			68, 70, 71, 72, 73, 74, 75, 76, 80, 84, 87, 89, 92, 94, 96, 97, 98,
			100, 101, 102, 105, 108, 112};

	union ui8_p8 uA, uZ;
	uint_fast8_t uiA;
	uA.p = pA;

	uZ.ui = (uA.ui>127) ? (0x8000) : (log8[uA.ui]);
	return uZ.p;
}
