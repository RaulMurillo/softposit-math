
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "internals.h"

uint_fast64_t p16_tanpipoly( uint_fast64_t );

posit16_t p16_tanpi( posit16_t pA ) {

	union ui16_p16 uA;
	uint_fast16_t uiA;
	uint_fast64_t bit, f, sign;
	int_fast64_t s;                 // s can be negative

	uA.p = pA;
	uiA = uA.ui;
	f = uiA;
	sign = f & 0x8000;
	if (sign) f = 0x10000 - f;      // 2's complement if negative
	if (f > 31743) {                // input value is an integer?
		uA.ui = (f == 0x8000) ? f : 0;
		return uA.p;            // handles NaR and integer cases
	}
	if (f) {                        // decode posit to fixed-point
		if (f & 0x4000) {
			s = 16;
			while ( f & 0x2000 ) {
				f = f << 1;
				s += 2;
			}
		} else {
			s = 14;
			while  ( !(f & 0x2000) ) {
				f = f << 1;
				s -= 2;
			}
		}
		if ( f & 0x1000 ) s++;      // decode exponent
		f = (f & 0x0FFF) | 0x1000;  // get 12-bit fraction; restore hidden bit
		f = (s < 0) ? f >> -s : f << s;
	}
	f = (f & 0xFFFFFFF);            // 28-bit fraction

	if (!f) {                       // tanpi is zero for integer inputs
		uA.ui = 0;
		return uA.p;
	}
	s = (f >> 27);                  // record quadrant = multiple of 1/2
	f = (f & 0x7FFFFFF);            // input modulo 1/2
	if (!f) {                       // tanpi is NaR at 1/2 * odd integers
		uA.ui = 0x8000;
		return uA.p;
	}
	if (s) sign = (sign ^ 0x8000);  // flip sign for odd quadrants
	if (!(f & 0x3FFFFFF)) {         // tanpi is +1 or -1 at 1/4 * odd integers
		uA.ui = (sign | 0x4000);
		return uA.p;
	}
	if (sign) f = 0x8000000 - f;            // reverse input direction for odd quadrants
	if (uiA & 0x8000) f = 0x8000000 - f;    // reverse if original input value is negative

	f = p16_tanpipoly(f);           // apply the polynomial approximation

	if (f > 0xFFFFFFF) {            // convert 28-bit fixed-point to a posit
		s = 12;
		while (!(f & 0x10000000000)) {
			f = f << 1;
			s--;
		}
		if (!(s & 1)) f = (f & 0xFFFFFFFFFF);
		s = (s >> 1) + 28;
		f = (((uint_fast64_t)2 << (14 + s)) - ((uint_fast64_t)1 << 42)) | f;
	} else {
		s = 1;
		while (!(f & 0x8000000)) {
			f = f << 1;
			s++;
		}
		bit = (s & 1);
		s = (s >> 1) + 14 + bit;
		if (!bit) f = (f & 0x7FFFFFF);
		f = (f | 0x10000000);
	}
	bit = ((uint_fast64_t)1 << (s - 1));
	if (f & bit) {                          // round to nearest, tie to even
		if ( (f & (bit - 1)) || (f & (bit << 1)) ) f += bit;
	}
	f = (f >> s);
	uA.ui = (sign) ? (0x10000 - f) : f;
	return uA.p;

}


uint_fast64_t p16_tanpipoly( uint_fast64_t f) {

	uint_fast64_t den, fs, fsq, num;

	if (f < 0xE001) return (f * 102943) >> 15;  // linear approximation suffices
	fs = f >> 9;
	fsq = (fs*fs) >> 10;

	// alternating num, den may help superscalar speed
	num = (fsq * 182527) >> 27;
	den = (fsq * 13335493) >> 25;
	num = fsq * (3648552 - num) >> 23;
	den = 0x8000000 - ((fsq * (295106440 - den)) >> 27);
	num = fs * (105414368 - num) << 11;
	return (num / den);

}
