/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#include "internals.h"

uint_fast64_t p16_asinpipoly(uint_fast64_t);
static uint_fast64_t isqrt(uint_fast64_t);


posit16_t p16_asinpi(posit16_t pA) {

    union ui16_p16 uA;
    uint_fast16_t uiA;
    uint_fast64_t bit, f;
    int_fast64_t s;                    // s can be negative

    uA.p = pA;
    uiA = uA.ui;
    if (!(uiA & 0x7FFF)) return uA.p;  // Handle 0 and NaR exceptions
    f = (uiA & 0x8000) ? 0x10000 - uiA : uiA;
    if (f > 0x4000) {                  // return NaR unless -1 <= input <= 1
        uA.ui = 0x8000;
        return uA.p;
    }
    if (f < 0x3000) {                  // input is less than 1/2
        s = 14;                        // convert to 28-bit fixed point
        while (!(f & 0x2000)) {
            f <<= 1;
            s -= 2;
        }
        if (f & 0x1000) s++;
        f = ((f & 0xFFF) | 0x1000);
        f = (s < 0) ? (f >> -s) : (f << s);
        f = p16_asinpipoly(f);
    } else {
        f = 0x20000000 - (p16_asinpipoly(isqrt((0x4000 - f) << 42)) << 1);
    }
    s = 34;                            // convert to posit form
    if (f > 4) {
        while (!(f & 0x20000000)) {
            f <<= 1;
            s++;
        }
        f = ((f ^ 0x60000000) | ((1 ^ (s & 1)) << 29));
        s >>= 1;
        bit = ((uint_fast64_t) 1 << (s - 1));
        if (f & bit) {                // round to nearest, tie to even
            if ((f & (bit - 1)) || (f & (bit << 1)))
                f += bit;
        }
        f >>= s;
    }
    uA.ui = (uiA >> 15) ? 0x10000 - f : f;
    return uA.p;
}


uint_fast64_t p16_asinpipoly(uint_fast64_t f) {

    uint_fast64_t fsq, s;

    fsq = (f * f) >> 28;
    s = 13944 + ((fsq * 3855) >> 26);
    s = 100344 + ((fsq * s) >> 26);
    s = 1780112 + ((fsq * s) >> 25);
    s = 42722832 + ((fsq * s) >> 26);
    return (f * s) >> 25;
}


static uint_fast64_t isqrt(uint_fast64_t f) {

    uint_fast64_t bit = 0x40000000000000, n, res = 0;

    n = f;
    while (bit > n) bit = (bit >> 2);
    while (bit) {
        if (n >= res + bit) {
            n -= (res + bit);
            res = (res >> 1) + bit;
        } else res = (res >> 1);
        bit = (bit >> 2);
    }
    return res;
}
