/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#include "internals.h"

uint_fast64_t p16_acospipoly(uint_fast64_t);
static uint_fast64_t isqrt(uint_fast64_t);
uint_fast64_t p16ToFixed28Acospi(uint_fast64_t);


posit16_t p16_acospi(posit16_t pA) {

    union ui16_p16 uA;
    uint_fast16_t uiA;
    uint_fast64_t bit, f, s;

    uA.p = pA;
    uiA = uA.ui;
    f = uiA;
    if (f > 0x4000 && f < 0xC000) {    // return NaR unless -1 <= input <= 1
        uA.ui = 0x8000;
        return uA.p;
    }
    if (f < 165 || f > 65307) {        // return 1/2 for inputs near 0
        uA.ui = 0x3000;
        return uA.p;
    }
    if (f < 0x3000) {                  // input is less than 1/2
        f = p16_acospipoly(p16ToFixed28Acospi(f));
    } else if (f < 0x4001) {
        f = 0x40000000 - (p16_acospipoly(isqrt((0x4000 - f) << 42)) << 1);
    } else if (f > 53248) {
        f = 0x40000000 - p16_acospipoly(p16ToFixed28Acospi(0x10000 - f));
    } else {
        f = (p16_acospipoly(isqrt((f - 0xC000) << 42)) << 1);
    }
    s = 35;                            // convert to posit form
    if (f > 1) {
        while (!(f & 0x40000000)) {
            f = f << 1;
            s++;
        }
        f = ((f ^ 0xC0000000) | ((1 ^ (s & 1)) << 30));
        s = (s >> 1);
        bit = ((uint_fast64_t) 1 << (s - 1));
        if (f & bit) {                // round to nearest, tie to even
            if ((f & (bit - 1)) || (f & (bit << 1)))
                f += bit;
        }
        f = (f >> s);
    }
    uA.ui = f;
    return uA.p;
}


uint_fast64_t p16_acospipoly(uint_fast64_t f) {

    uint_fast64_t fsq, s;

    fsq = (f * f) >> 28;
    s = 13696 + ((fsq * 7955) >> 27);
    s = 100510 + ((fsq * s) >> 26);
    s = 1780047 + ((fsq * s) >> 25);
    s = 42722829 + ((fsq * s) >> 26);
    return 0x20000000 - ((f * s) >> 25);
}



static uint_fast64_t isqrt(uint_fast64_t f) {

    uint_fast64_t bit = 0x40000000000000, n, res = 0;

    n = f;
    while (bit > n) bit = (bit >> 2);
    while (bit) {
        if (n >= res + bit) {
            n -= (res + bit);
            res = (res >> 1) + bit;
        } else res = (res >> 1);
        bit = (bit >> 2);
    }
    return res;
}


uint_fast64_t p16ToFixed28Acospi(uint_fast64_t i) {

    uint_fast64_t f, s = 14;

    f = i;
    while (!(f & 0x2000)) {
        f = (f << 1);
        s -= 2;
    }
    if (f & 0x1000) s++;
    f = (f & 0xFFF) | 0x1000;
    return (f << s);
}
