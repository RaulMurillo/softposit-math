
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the University nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS", AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "internals.h"

uint_fast64_t p16_exp2poly( uint_fast64_t );

posit16_t p16_exp2( posit16_t pA ){

	union ui16_p16 uA;
	uint_fast16_t uiA;
	uint_fast64_t bit, f, s = 0;

	uA.p = pA;
	uiA = uA.ui;
	f = uiA;

	// Calculate the exponential for given posit pA
	if ( uiA < 29377 ) {				// result does not round up to maxpos

		if ( uiA < 221 ) {			// cases that round down to 1.
			uA.ui = 0x4000;
			return uA.p;
		}

		if ( f & 0x4000 ) {			// decode regime
			s = 8;
			while (f & 0x2000) {
				f = f << 1;
				s += 2;
			}
		} else {
			s = 6;
			while ( !(f & 0x2000) ) {
				f = f << 1;
				s -= 2;
			}
		}

		if (f & 0x1000) s++;			// decode exponent
		f = (f & 0x0FFF) | 0x1000;		// decode fraction
		f = ((int)s < 0 ? f >> -s : f << s);
		s = f >> 20;				// s now stores floor(x)
		f = p16_exp2poly(f & 0xFFFFF);		// fraction bits of exp2(x)
		bit = (s & 1) << 26;			// exponent bit of exp2(x)
		s = s >> 1;				// regime length of exp2(x)
		f = (((uint_fast64_t)0x20000000 << s) - 0x10000000) | bit | f;

		bit = (uint_fast64_t)1 << (13 + s);	// location of bit n-plus-1
		if ( f & bit ) {
			if ((f & (bit - 1)) || (f & (bit << 1))) {
				f += bit;
			}
		}
		uA.ui = f >> (14 + s);
		return uA.p;				// return rounded exp2(x) as posit

	} else if (uiA > 36159) {

		if (uiA > 65379 ) {			// cases that round up to 1.
			uA.ui = 0x4000;
			return uA.p;
		}
		if (f & 0x4000) {			// decode regime
			s = 7;
			while (f & 0x2000) {
				f = f << 1;
				s -= 2;
			}
		} else {
			s = 9;
			while (!(f & 0x2000)) {
				f = f << 1;
				s += 2;
			}
		}

		if (f & 0x1000) s--;			// decode exponent
		f = (f & 0xFFF) | 0x1FFE000;		// decode fraction
		f = ((int)s < 0) ? (f >> -s) | (0x2000000 - (1 << (13 + s))) : (f << s) & 0x1ffffff;
		s = (f >> 20) - 32;			// s now stores floor(x)
		f = p16_exp2poly(f & 0xFFFFF);		// fraction bits of exp2(x)
		bit = (s & 1) << 26;			// exponent bit of exp2(x)
		s = (-1 - s) >> 1;
		f = 0x8000000 | bit | f;		// Install regime end bit

		bit = (uint_fast64_t)1 << (13 + s);	// location of bit n-plus-1
		if ( f & bit ) {
			if ((f & (bit - 1)) || (f & (bit << 1))) {
				f += bit;
			}
		}
		uA.ui = f >> (14 + s);
		return uA.p;			// return rounded exp2(x) as posit

	}

	// Section for exception cases
	if ( uiA < 0x8000 ) {			// cases that round to maxpos
		uA.ui = 0x7FFF;
		return uA.p;
	} else if ( uiA > 0x8000 ) {		// cases that round to minpos
		uA.ui = 0x0001;
		return uA.p;
	} else {
		uA.ui = 0x8000;			// NaR case
		return uA.p;
	}

}


uint_fast64_t p16_exp2poly( uint_fast64_t f ){

	uint_fast64_t s = 0;

	s = (f * (0x9BA00000 + (f * 491))) >> 34;
	s = (f * (0x13F840 + s)) >> 20;
	s = (f * (0x718A80 + s)) >> 16;
	s = (f * (0x1EC04000 + s)) >> 21;
	s = ((f * (0x2C5C8000 + s)) >> 24);

	return s;
}
