
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/


#include "internals.h"

uint_fast64_t p16_atanpipoly(uint_fast64_t);

posit16_t p16_atanpi(posit16_t pA) {

    union ui16_p16 uA;
    uint_fast16_t uiA;
    uint_fast64_t bit, f;
    int_fast64_t s;                     // s can be negative

    uA.p = pA;
    uiA = uA.ui;
    f = uiA;
    if (!(f & 0x7FFF))
        return uA.p;                    // dispense with NaR and 0 cases

    if (uiA >> 15) f = 0x10000 - f;     // f = |f|

    if (f < 0x4000) {                   // SE quadrant; regime bit is 0
        if (f > 4925) {
            s = 14;
            while (!(f & 0x2000)) {     // decode regime
                f = (f << 1);
                s -= 2;
            }
            if (f & 0x1000) s++;        // decode exponent
            f = (f & 0x0FFF) | 0x1000;  // get 12-bit fraction and restore hidden bit
            f = (f << s);
            f = p16_atanpipoly(f);
        } else {                        // use small x approximation
            s = 13;
            while (!(f & 0x1000)) {     // decode regime
                f = (f << 1);
                s -= 2;
            }
            if (f & 0x800) s++;         // decode exponent
            f = (f & 0x7FF) | 0x800;    // get 12-bit fraction and restore hidden bit
            f = (s < 0) ? (f >> -s) : (f << s);
            f = (f << 30) / (((f*f >> 34) * 67) + 843314118);
        }
    } else {                            // NE quadrant; regime bit is 1
        if (f < 27109) {
            s = 0;
            while (f & 0x2000) {        // decode regime
                f = (f << 1);
                s += 2;
            }
            if (f & 0x1000) s++;        // decode exponent
            f = (f & 0x0FFF) | 0x1000;  // get 12-bit fraction and restore hidden bit
            f = (f << s);
            f = (0x10000000000 / f);    // fixed-point reciprocal
            f = 0x20000000 - p16_atanpipoly(f);
        } else {
            s = -1;
            while (f & 0x1000) {        // decode regime
                f = (f << 1);
                s += 2;
            }
            if (f & 0x800) s++;         // decode exponent
            f = (f & 0x7FF) | 0x800;    // get 12-bit fraction and restore hidden bit
            f = (f << s);               // use large value approx. on fixed point:
            f = 0x20000000 - (0x28BE5FF800000 / ((f << 13) + (0xAA55000 / f)));
        }
    }

    // convert fixed-point to a posit
    if (f > 1) {                        // leave f = 0 and f = minpos alone
        s = 34;
        while (!(f & 0x20000000)) {
            f = f << 1;
            s++;
        }
        f = ((f ^ 0x60000000) | ((1 ^ (s & 1)) << 29));
        s = (s >> 1);
        bit = ((uint_fast64_t) 1 << (s - 1));
        if (f & bit) {                  // round to nearest, tie to even
            if ((f & (bit - 1)) || (f & (bit << 1)))
                f += bit;
        }
        f = (f >> s);
    }
    uA.ui = (uiA >> 15) ? (0x10000 - f) : f;
    return uA.p;

}

uint_fast64_t p16_atanpipoly(uint_fast64_t f) {

    uint_fast64_t fsq, s;

    fsq = (f * f) >> 28;
    s = (fsq * 6969) >> 24;
    s = (fsq * (530432 - s)) >> 28;
    s = (fsq * (1273944 - s)) >> 28;
    s = (fsq * (2358656 - s)) >> 27;
    s = (fsq * (9340208 - s)) >> 29;
    s = (fsq * (17568064 - s)) >> 24;

    return ((f + 1) << 30) / (843315168 + s);

}
