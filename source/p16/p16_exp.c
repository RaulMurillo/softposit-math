
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the University nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS", AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "internals.h"

uint_fast64_t p16_exppoly( uint_fast64_t );

posit16_t p16_exp( posit16_t pA ){

	union ui16_p16 uA;
	uint_fast16_t uiA;
	uint_fast64_t bit, f, s = 0;

	uA.p = pA;
	uiA = uA.ui;
	f = uiA;

	// Calculate the exponential for given posit pA
	if ( uiA < 28846 ) {		// result does not round up to maxpos

		if ( uiA < 192 ) {	// small positive values that round to 1
			uA.ui = 0x4000;
			return uA.p;
		}

		if ( f & 0x4000 ) {	// decode regime
			s = 8;
			while (f & 0x2000) {
				f = f << 1;
				s += 2;
			}
		} else {
			s = 6;
			while ( !(f & 0x2000) ) {
				f = f << 1;
				s -= 2;
			}
		}

		if (f & 0x1000) s++;					// decode exponent
		f = (f & 0x0FFF) | 0x1000;				// decode fraction
		f = (((int)s < 0 ? f >> -s : f << s) * 48408813) >> 20;
		s = f >> 25;						// s now stores floor(x)
		f = p16_exppoly(f & 0x1FFFFFF);				// 37 fraction bits of exp(x)
		bit = (s & 1) << 37;					// exponent bit of exp(x)
		s = s >> 1;						// regime length of exp(x)
		f = ((0x10000000000 << s) - 0x8000000000) | bit | f;

		bit = (uint_fast64_t)1 << (24+s);			// location of bit n-plus-1
		if ( f & bit ) {
			if ((f & (bit - 1)) || (f & (bit << 1))) {
				f += bit;
			}
		}
		uA.ui = f >> (25 + s);
		return uA.p;		// return rounded exp(x) as posit

	} else if (uiA > 36690) {	// result does not round up to minpos

		if (uiA > 65407) {	// small negative values that round to 1
			uA.ui = 0x4000;
			return uA.p;
		}

		if (f & 0x4000) {	// decode regime
			s = 7;
			while (f & 0x2000) {
				f = f << 1;
				s -= 2;
			}
		} else {
			s = 9;
			while (!(f & 0x2000)) {
				f = f << 1;
				s += 2;
			}
		}

		if (f & 0x1000) s--;				// decode exponent
		f = (f & 0x0FFF) | 0x1FFE000;			// decode fraction
		f = ((int)s < 0) ? (f >> -s) | (0x2000000 - (1 << (13 + s))) : (f << s) & 0x1ffffff;
		f = (0x4000000000000 - ((0x2000000 - f) * 48408813)) >> 20;
		s = (f >> 25) - 32;				// s now stores floor(x)
		f = p16_exppoly(f & 0x1FFFFFF);			// 37 fraction bits of exp(x)
		bit = (s & 1) << 37;				// exponent bit of exp(x)
        	s = (-1 - s) >> 1;
		f = 0x4000000000 | bit | f;			// Install regime end bit

		bit = (uint_fast64_t)1 << (24+s);		// location of bit n-plus-1
		if ( f & bit ) {
			if ((f & (bit - 1)) || (f & (bit << 1))) {
				f += bit;
			}
		}
		uA.ui = f >> (25 + s);
		return uA.p;	// return rounded exp(x) as posit

	}

	// Section for exception cases
	if (uiA < 0x8000) {
		uA.ui = 0x7FFF;
		return uA.p;		// return maxpos
	} else if (uiA > 0x8000) {
		uA.ui = 0x0001;
		return uA.p;		// return minpos
	} else {
		uA.ui = 0x8000;
		return uA.p;		// return NaR
	}

}


uint_fast64_t p16_exppoly( uint_fast64_t f ){

	uint_fast64_t s = 0;

	s = (f * 7529) >> 26;
	s = (f * (20487 + s)) >> 20;
	s = (f * (0x4F8300 + s)) >> 24;
	s = (f * (0x38CC980 + s)) >> 20;
	s = (f * (0x1EBFFC800 + s)) >> 26;
	s = ((f * (0x2C5C83600 + s)) >> 22) + 2048;

	return s;
}
