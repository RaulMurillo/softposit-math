
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "internals.h"

uint_fast64_t p16_cospipoly(uint_fast64_t);

posit16_t p16_cospi(posit16_t pA) {

	union ui16_p16 uA;
	uint_fast16_t uiA;
	uint_fast64_t bit, f, sign = 0;
	int_fast64_t s;                     // s can be negative

	uA.p = pA;
	uiA = uA.ui;
	f = uiA;
	if (f == 0x8000)
		return uA.p;                // dispense with the NaR case

	if (f & 0x8000) f = 0x10000 - f;    // f = |f|

	if (f) {
		if (f & 0x4000) {           // decode regime
			s = 16;
			while (f & 0x2000) {
				f = f << 1;
				s += 2;
			}
		} else {
			s = 14;
			while (!(f & 0x2000)) {
				f = f << 1;
				s -= 2;
			}
		}
		if (f & 0x1000) s++;        // decode exponent
		f = (f & 0x0FFF) | 0x1000;  // get 12-bit fraction and restore hidden bit
		f = (s < 0) ? f >> -s : f << s;
	}
	s = f >> 27;                    // the quadrant is the multiple of 1/2
	f = f & 0x7FFFFFF;              // input value modulo 1/2
	if ((s + 1) & 2) sign = 0x8000; // cos is negative for quadrants 2 and 3
	if (!f) {
		uA.ui = (s & 1) ? 0 : (sign | 0x4000);
		return uA.p;
	}
	if (s & 1) f = 0x8000000 - f;
	f = p16_cospipoly(f);
	s = 1;                          // convert fixed-point to a posit
	while (!(f & 0x1000000)) {
		f = f << 1;
		s++;
	}
	bit = (s & 1);
	if (!bit) f = (f & 0xFFFFFF);   // encode exponent bit
	s = (s >> 1) + 12;
	if (!bit) s--;

	f = (f | 0x2000000);            // encode regime termination bit
	bit = ((uint_fast64_t)1 << (s - 1));
	if (f & bit) {                  // round to nearest, tie to even
		if ((f & (bit - 1)) || (f & (bit << 1))) f += bit;
	}
	f = (f >> s);
	uA.ui = (sign) ? (0x10000 - f) : f;
	return uA.p;

}


uint_fast64_t p16_cospipoly(uint_fast64_t f) {

	uint_fast64_t fsq, s;

	if (f < 0xE6001) return 0x1FFFFFF;  // this rounds up to 1.0
	fsq = (f >> 11);                    // convert to 17-bit fixed point
	fsq = ((fsq * fsq) >> 8);
	s = 349194 - ((fsq * 28875) >> 25);
	s = 4255560 - ((fsq * s) >> 24);
	s = 20698014 - ((fsq * s) >> 24);
	return 33554428 - ((fsq * s) >> 23);

}
