#include "positMath.h"
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]){

    posit16_t pA, pZ;
    double time;
    int i=0, numthreads;
    unsigned long j=0, end;

#pragma omp parallel
#pragma omp master
    numthreads = omp_get_num_threads();

    end = numthreads*24000;

    printf("Benchmarking tanpi posit16 operations using %d threads.\n", numthreads);
    time = omp_get_wtime();

#pragma omp parallel for private(i,pA,pZ)
    for (j=0; j<end; j++) {

		for (i=0; i<65536; i++)
		{
			pA.v = i;
			pZ = p16_tanpi(pA);
		}
		if (numthreads < 1) printf("Dummy line");       // Prevent compiler optimising out loop

    }
    time = omp_get_wtime() - time;

    printf("Done %lu tanpi posit16 operations in %f seconds, using %d threads.\n", end * 65536, time, numthreads);
    printf("Speed is %.0f pop/s.\n", (double) (end * 65536)/time);

    return 0;

}

